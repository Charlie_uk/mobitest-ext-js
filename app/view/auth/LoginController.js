Ext.define('Mobitest.view.auth.LoginController', {
  extend: 'Ext.app.ViewController',
  alias: 'controller.authlogin',

  // Insert test user details for rpaid login
  init () {
    const me = this;
    const form = me.lookup('form');
    form.setValues({
      username: 'admin',
      password: 'bossman'
    });
  },

  onLoginTap: function() {

    const me = this;
    const form = me.lookup('form');
    const values = form.getValues();

    form.clearErrors();

    Ext.Viewport.setMasked({ xtype: 'loadmask' });

    UserSession.login(values.username, values.password)
      .then((user) => {
        me.getView().fireEvent('login');
      })
      .catch((errors) => { form.setErrors(errors); })
      .then(() => { Ext.Viewport.setMasked(false); });

  }
});

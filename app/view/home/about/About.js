/**
 * The application home screen. Provides access to the main functions of the application via a side/
 * top menu bar.
 */
Ext.define('Mobitest.view.about.About', {
  extend: 'Ext.Panel',
  xtype: 'mobi-about',
  cls: 'mobi-about',
  html: `
    <div class="welcome-container">
      <h1>Welcome to MobiTest!</h1>
      <p>
        This application is intended as demonstration of a reactive web and mobile application built using
        Ext JS 6.6 and wrapped in Cordova for use on Andorid and Windows mobile devices.
      </p>
      <p>
        Selecting any item from the menu will show an example of how that functionality can respond to changes
        in layout, device, or device capability.
      </p>
      <p>
        The menu itself behaves in a responsive manner and is hidden behind a menu button on phones and portrait
        tablets, and embedded on the home screen otherwise,
      </p>
      <p>
        Where possible the Ext JS reponsive plugin or mixin is used to respond to changes in preference to
        css media queries.
      </p>
    </div>
  `
});

/**
 * View model for the application home screen
 */
Ext.define('Mobitest.view.home.HomeModel', {
  extend: 'Ext.app.ViewModel',
  alias: 'viewmodel.home',

  stores: {
    menuOptions: {
        model: 'Mobitest.model.MenuOption',
        data : [
          { text: 'About', icon: 'fa-info-circle', xtype: 'mobi-about'},
          { text: 'Form', icon: 'fa-wpforms', xtype: 'mobi-exampleform'},
          { text: 'Dialog', icon: 'fa-star', xtype: 'mobi-exampledialog'},
          { text: 'Image Capture', icon: 'fa-camera', xtype: 'mobi-exampleimagecapture'},
          { text: 'Notification', icon: 'fa-bell', xtype: ''},
          { text: 'Field Containers', icon: 'fa-wpforms', xtype: 'mobi-examplefieldgroups'},
          { text: 'Tab', icon: 'fa-table', xtype: 'mobi-exampletab'},
          { text: 'Visitors', icon: 'fa-database', xtype: 'mobi-complexdata'}
        ]
    }
  }

});

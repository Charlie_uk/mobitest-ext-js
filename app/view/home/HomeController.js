/**
 * Controller for the application home screen
 */
Ext.define('Mobitest.view.home.HomeController', {
  extend: 'Ext.app.ViewController',
  alias: 'controller.home',

  init () {

    const me = this;
    const menuOptions = me.getStore('menuOptions');

    me.initialiseViewPortMenu();
    me.addCardViews();

  },

  initialiseViewPortMenu () {

    const me = this;
    const menuOptions = me.getStore('menuOptions');

    me._menu = Ext.create({
      xtype: 'actionsheet',
      cls: 'mobi-slide-out-menu',
      side: 'left',
      width: 200,
      cover: true,
      items: [
        {
          xtype: 'sidemenu',
          title: 'MobiTest',
          height: '100%',
          width: '100%',
          menuOptions: menuOptions,
          listeners: {
            menuItemSelected: me.onMenuItemSelected.bind(me),
            logout: me.onLogout.bind(me)
          }
        }
      ]
    });

    Ext.Viewport.setMenu(me._menu);
    Ext.Viewport.on('orientationchange', me.hideSideMenu, me);

  },

  addCardViews () {

    const me = this;
    const contentContainer = me.lookup('content');
    const menuOptions = me.getStore('menuOptions');

    menuOptions.each(option => {
      if (!option.get('xtype')) { return; }
      contentContainer.add({
        xtype: option.get('xtype'),
        reference: option.get('xtype')
      })
    });

  },

  showSideMenu () {
    Ext.Viewport.showMenu('left');
  },

  hideSideMenu () {
    Ext.Viewport.hideMenu('left');
  },

  onMenuItemSelected (menuOption) {
    const me = this;
    const cmp = me.lookup(menuOption.get('xtype'));
    const contentContainer = me.lookup('content');
    contentContainer.setActiveItem(cmp);
    Ext.Viewport.hideMenu('left');
  },

  onLogout () {
    this.getView().fireEvent('logout');
    Ext.Viewport.hideMenu('left');
  }

});

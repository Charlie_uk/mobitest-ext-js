/**
 * The application home screen. Provides access to the main functions of the application via a side/
 * top menu bar.
 */
Ext.define('Mobitest.view.home.Home', {
  extend: 'Ext.Container',
  xtype: 'home',

  controller: 'home',
  viewModel: 'home',

  cls: 'home',

  layout: {
    type: 'hbox'
  },

  items: [
    {
      xtype : 'titlebar',
      docked: 'top',
      innerCls: 'home-titlebar',
      title: 'Mobitest',
      items: [
        {
          iconCls: 'x-fa fa-bars',
          align: 'left',
          handler: 'showSideMenu',
          plugins: {
            responsive: true
          },
          responsiveConfig: {
            desktop: {
              hidden: true
            },
            'tablet && portrait': {
              hidden: false
            },
            'tablet && landscape': {
              hidden: true
            }
          }
        },
        {
          xtype: 'component',
          align: 'right',
          cls: 'platform-label',
          plugins: {
            responsive: true
          },
          responsiveConfig: {
            phone: {
              html: 'Platform: Mobile'
            },
            desktop: {
              html: 'Platform: Desktop'
            },
            tablet: {
              html: 'Platform: Tablet'
            }
          },
        }
      ]
    },
    {
      xtype : 'sidemenu',
      bind: {
        menuOptions: '{menuOptions}'
      },
      plugins: {
        responsive: true
      },
      responsiveConfig: {
        desktop: {
          hidden: false
        },
        phone: {
          hidden: true
        },
        'tablet && portrait': {
          hidden: true
        },
        'tablet && landscape': {
          hidden: false
        }
      },
      listeners: {
        menuItemSelected: 'onMenuItemSelected',
        logout: 'onLogout'
      },
      width: 200
    },
    {
      xtype : 'container',
      reference: 'content',
      layout: {
        type: 'card',
        animation : {
          type      : 'slide',
          direction : 'vertical',
          duration  : 350,
        }
      },
      flex: 1
    }
  ]

});

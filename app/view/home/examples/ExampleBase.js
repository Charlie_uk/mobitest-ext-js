/**
 * Example base class - Examples should extend this class to inherit common styling.
 */
Ext.define('Mobitest.view.examples.ExampleBase', {
  extend: 'Ext.Panel',
  cls: 'mobi-examplebase',
  padding: 20
});

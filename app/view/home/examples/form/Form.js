/**
 * Example base class - Examples should extend this class to inherit common styling.
 */
Ext.define('Mobitest.view.examples.form.Form', {
  extend: 'Mobitest.view.examples.ExampleBase',
  xtype: 'mobi-exampleform',
  cls: 'mobi-exampleform',

  scrollable: 'vertical',

  items: [
    {
      xtype: 'mobi-infopanel',
      title: 'Form Demo',
      html: `
        <p>The form below is configured to change the label alignment of each field as the viewport moves between a configured break point of 768px.</p>
      `
    },
    {
      xtype: 'formpanel',

      defaults: {
        plugins: {
          responsive: true
        }
      },

      /*
       * Applying the resposive config could probably done in a nicer way on init or with a mixin but this will do
       * for demo purposes
       */

      items: [
        {
          xtype: 'textfield',
          name: 'surname',
          label: 'Surname',
          responsiveConfig: { 'width < 768': { labelAlign: 'top' }, 'width >= 768': { labelAlign: 'left' } }
        },
        {
          xtype: 'textfield',
          name: 'forename',
          label: 'Forename',
          responsiveConfig: { 'width < 768': { labelAlign: 'top' }, 'width >= 768': { labelAlign: 'left' } }
        },
        {
          xtype: 'datefield',
          name: 'dateofrecording',
          label: 'Date of Recording',
          responsiveConfig: { 'width < 768': { labelAlign: 'top' }, 'width >= 768': { labelAlign: 'left' } }
        },
        {
          xtype: 'timefield',
          name: 'timeofrecording',
          label: 'Time of Recording',
          responsiveConfig: { 'width < 768': { labelAlign: 'top' }, 'width >= 768': { labelAlign: 'left' } }
        },
        {
          xtype: 'passwordfield',
          name: 'password',
          label: 'Password',
          responsiveConfig: { 'width < 768': { labelAlign: 'top' }, 'width >= 768': { labelAlign: 'left' } }
        }
      ]

    }
  ]

});

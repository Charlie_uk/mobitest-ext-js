/**
 * Example base class - Examples should extend this class to inherit common styling.
 */
Ext.define('Mobitest.view.examples.fieldgroups.FieldGroups', {
  extend: 'Mobitest.view.examples.ExampleBase',
  xtype: 'mobi-examplefieldgroups',
  cls: 'mobi-examplefieldgroups',

  scrollable: 'vertical',

  items: [
    {
      xtype: 'mobi-infopanel',
      title: 'Field Container Demo',
      html: `
        <p>
          The form below uses field containers to wrap multiple fields to display on a single line.
          At a configured break point of 768px the field containers and contained fields reconfigure
          to display in a vertical orientation with individual field labels.
        </p>
      `
    },
    {
      xtype: 'formpanel',

      /*
       * Applying the resposive config could probably done in a nicer way on init or with a mixin but this will do
       * for demo purposes
       */

      items: [
        // 3 individual fields in a container
        {
          xtype: 'containerfield',
          plugins: {
            responsive: true
          },
          responsiveConfig: {
            'width < 768': { layout: { vertical: true }, label: false },
            'width >= 768': { layout: { vertical: false }, label: 'Names (Forename, Middle, Surname)' }
          },
          defaults:{
            plugins: {
              responsive: true
            },
            labelAlign: 'left'
          },
          items: [
            {
              flex: 1,
              responsiveConfig: { 'width < 768': { label: 'First' }, 'width >= 768': { label: false } }
            },
            {
              flex: 0.6,
              responsiveConfig: { 'width < 768': { label: 'Middle', margin: false }, 'width >= 768': { label: false, margin: '0 10' } }
            },
            {
              flex: 1,
              responsiveConfig: { 'width < 768': { label: 'Last' }, 'width >= 768': { label: false } }
            }
          ]
        },
        // 2 child field containers within an outer field container
        {
          xtype: 'containerfield',
          plugins: {
            responsive: true
          },
          responsiveConfig: {
            'width < 768': { layout: { vertical: true }, label: false },
            'width >= 768': { layout: { vertical: false }, label: 'Date / Time' }
          },
          margin: '20 0',
          defaults:{
            plugins: {
              responsive: true
            },
            labelAlign: 'left',
            flex: 1
          },
          items: [
            {
              xtype: 'containerfield',
              responsiveConfig: { 'width < 768': { label: 'Date' }, 'width >= 768': { label: false } },
              items: [
                {
                  reference: 'date',
                  flex: 1
                },
                {
                  xtype: 'button',
                  text: 'today',
                  flex: 1,
                  maxWidth: 80,
                  ui: 'action',
                  margin: '0 0 0 10',
                }
              ]
            },
            {
              xtype: 'containerfield',
              responsiveConfig: { 'width < 768': { label: 'Time', margin: false }, 'width >= 768': { label: false, margin: '0 0 0 10' } },
              items: [
                {
                  reference: 'time',
                  flex: 1
                },
                {
                  xtype: 'button',
                  text: 'now',
                  flex: 1,
                  maxWidth: 80,
                  ui: 'action',
                  margin: '0 0 0 10'
                }
              ]
            }
          ]
        }
      ]
    }

  ]

});

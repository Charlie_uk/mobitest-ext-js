/**
 * Code Panel - Displays styled code using Prism.js
 */
Ext.define('Mobitest.view.examples.CodePanel', {
  extend: 'Ext.Panel',
  xtype: 'mobi-codepanel',
  cls: 'mobi-codepanel',
  ui: 'mobi-codepanel',

  config: {

    /**
     * @cfg {String} code (required)
     * The code to display
     */
    code: '',

    /**
     * @cfg {String} language (required)
     * The code language see https://prismjs.com/ for langauge aliases
     */
    language: ''

  },

  listeners: {
    painted: {
        fn: function () { this.applyPrismFormatting(); }
    }
  },

  iconCls: 'x-fa fa-code',
  tpl: '<pre><code class="language-{language}">{code}</code></pre>',

  tools: [
    {
      itemId: 'maximize',
      type: 'maximize',
      handler: function (panel) {

        const dialog = Ext.create({
          xtype: 'dialog',
          layout: 'fit',
          title: panel.getTitle(),
          maximized: true,
          maximizable: false,
          items: [
            {
              xtype: 'mobi-codepanel',
              header: false,
              scrollable: 'vertical',
              code: panel.getCode(),
              language: panel.getLanguage()
            }
          ],

          buttons: {
            ok: function () {  dialog.destroy(); }
          }

        }).show();

      }
    }
  ],

  initialize () {
    // Check for the presence of prism JS and raise error if unavailable
    if (!window.Prism) {
      Ext.raise('Prism is not available. Please ensure the Prism js and css files are include in the project');
    }
    this.setDataValues();
  },

  updateCode () {
    this.setDataValues();
  },

  updateLanguage () {
    this.setDataValues();
  },

  setDataValues() {
    this.setData({
      language: this.getLanguage(),
      code: this.getCode()
    });
    this.applyPrismFormatting();
  },

  applyPrismFormatting () {
    Prism.highlightAllUnder(this.element.dom);
  }

});

/**
 * Controller for the application home screen
 */
Ext.define('Mobitest.view.home.examples.dialog.DialogController', {
  extend: 'Ext.app.ViewController',
  alias: 'controller.dialog',

  onInitDialog () {

    const dialog = Ext.create({
      xtype: 'dialog',
      plugins: {
        responsive: true
      },

      layout: 'center',
      title: 'Example Dialog',
      width: 500,

      responsiveConfig: {
        'width < 768': {
          maximized: true,
          maximizable: false
        },
        'width >= 768': {
          maximized: false,
          maximizable: true
        }
      },

      items: [
        {
          xtype: 'component',
          html: `
            <p>If the viewport size becomes less that the width break point of 768px then this dialog should maximise to full screen.</p>
            <p>This should recalculate when resizing a browser window or when reorientating a mobile device.</p>
          `
        }
      ],

      buttons: {
        ok: function () {  dialog.destroy(); },
        cancel: function () {  dialog.destroy(); }
      }

    });

    dialog.show();

  }

});

/**
 * Example base class - Examples should extend this class to inherit common styling.
 */
Ext.define('Mobitest.view.examples.dialog.Dialog', {
  extend: 'Mobitest.view.examples.ExampleBase',
  xtype: 'mobi-exampledialog',
  controller: 'dialog',
  cls: 'mobi-exampledialog',

  items: [
    {
      xtype: 'mobi-infopanel',
      title: 'Dialog Demo',
      html: `
        <p>The dialog shown should maximise when the viewport is less than 768px, this should recalulate on device reorientation to avoid clipped dialogs</p>
      `
    },
    {
      xtype: 'container',
      layout: 'center',
      height: 200,
      items: [
        {
          xtype: 'button',
          text: 'Show Dialog',
          ui: 'action',
          handler: 'onInitDialog'
        }
      ]

    }
  ]

});

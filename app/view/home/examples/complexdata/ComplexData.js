/**
 * Example base class - Examples should extend this class to inherit common styling.
 */
Ext.define('Mobitest.view.home.examples.complexdata.ComplexData', {
  extend: 'Mobitest.view.examples.ExampleBase',
  xtype: 'mobi-complexdata',
  controller: 'complexdata',
  cls: 'mobi-complexdata',

  defaults: {
    margin: '0 0 10 0'
  },

  viewModel: 'complexdata',
  scrollable: 'vertical',

  items: [
    {
      xtype: 'mobi-infopanel',
      title: 'Dialog Demo',
      items: [
        {
          xtype: 'component',
          html: `
            <p>The examples below show the concept of applying visitors to nested JSON objects to
            work with data. The example data can be seen by presing the button below. Each example
            shows the vistitor used and the resulting output.</p>
          `
        },
        {
          xtype: 'container',
          layout: {
            type: 'hbox',
            align: 'end',
            pack: 'end'
          },
          items: [
            {
              xtype: 'button',
              text: 'Show Example Data',
              ui: 'action',
              handler: 'onShowExampleData'
            }
          ]
        }
      ]
    },
    {
      xtype: 'mobi-infopanel',
      title: 'String Conversion',
      iconCls: 'x-fa fa-eye',
      items: [
        {
          xtype: 'component',
          html: `<p>Traverse the data and convert into a basic indented string representation</p>`
        },
        {
          xtype: 'mobi-codepanel',
          title: 'Visitor - String Formatter',
          maxHeight: 300,
          scrollable: 'vertical',
          language: 'javascript',
          bind: {
            code: '{stringVisitor}'
          }
        },
        {
          xtype: 'mobi-codepanel',
          title: 'Results - String Formatter',
          maxHeight: 300,
          scrollable: 'vertical',
          language: 'json',
          bind: {
            code: '{stringVisitorResult}'
          },
          margin: '20 5 0 0'
        }
      ]
    },
    {
      xtype: 'mobi-infopanel',
      title: 'Count Traits',
      iconCls: 'x-fa fa-eye',
      items: [
        {
          xtype: 'component',
          html: `<p>Traverse the data and creates a count of how many times each triat is attributed
          to any character</p>`
        },
        {
          xtype: 'mobi-codepanel',
          title: 'Visitor - Trait Count',
          maxHeight: 300,
          scrollable: 'vertical',
          language: 'javascript',
          bind: {
            code: '{TraitCountVisitor}'
          }
        },
        {
          xtype: 'mobi-codepanel',
          title: 'Results - String Formatter',
          maxHeight: 300,
          scrollable: 'vertical',
          language: 'json',
          bind: {
            code: '{TraitCountVisitorResult}'
          },
          margin: '20 5 0 0'
        }
      ]
    }
  ]

});

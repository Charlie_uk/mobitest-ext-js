/**
 * Controller for the application home screen
 */
Ext.define('Mobitest.view.home.examples.complexdata.ComplexDataController', {
  extend: 'Ext.app.ViewController',
  alias: 'controller.complexdata',

  requires: [
    'Mobitest.data.Got',
    'Mobitest.util.hierarchicalvisitor.ObjectDispatcher',
    'Mobitest.util.hierarchicalvisitor.visitors.StringFormat',
    'Mobitest.util.hierarchicalvisitor.visitors.CountTraits'
  ],

  /**
   * Generate test results.
   */
  init () {

    const me = this;
    const viewModel = me.getViewModel();

    const objectDispatcher = Ext.create('Mobitest.util.hierarchicalvisitor.ObjectDispatcher');
    const stringFormatter = Ext.create('Mobitest.util.hierarchicalvisitor.visitors.StringFormat');
    const traitCounter = Ext.create('Mobitest.util.hierarchicalvisitor.visitors.CountTraits');

    objectDispatcher.dispatch(Mobitest.data.Got.info, stringFormatter);
    viewModel.set('stringVisitorResult', stringFormatter.getResult());

    objectDispatcher.dispatch(Mobitest.data.Got.info, traitCounter);
    viewModel.set('TraitCountVisitorResult', JSON.stringify(traitCounter.getResult(), null, 2));

  },

  onShowExampleData () {

    const exampleData = JSON.stringify(Mobitest.data.Got.info, null, 2);

    const dialog = Ext.create({
      xtype: 'dialog',
      layout: 'fit',
      title: 'Example Data',
      maximized: true,
      maximizable: false,
      items: [
        {
          xtype: 'mobi-codepanel',
          header: false,
          scrollable: 'vertical',
          code: exampleData,
          language: 'json'
        }
      ],

      buttons: {
        ok: function () {  dialog.destroy(); }
      }

    });

    dialog.show();

  }

});

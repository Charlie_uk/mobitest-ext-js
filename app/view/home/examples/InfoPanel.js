/**
 * Note panel - Displays styled notes
 */
Ext.define('Mobitest.view.examples.InfoPanel', {
  extend: 'Ext.Panel',
  xtype: 'mobi-infopanel',
  iconCls: 'x-fa fa-info-circle',
  cls: 'mobi-infopanel',
  ui: 'mobi-infopanel'
});

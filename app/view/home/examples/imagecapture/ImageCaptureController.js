/**
 * Controller for the image capture example
 */
Ext.define('Mobitest.view.examples.imagecapture.ImageCaptureController', {
  extend: 'Ext.app.ViewController',
  alias: 'controller.imagecapture',

  /**
   * Set the view model to represent the device capability so the view can configure appropriately.
   * @return {void}
   * @private
   */
  init () {
    const me = this;
    const cordovaEnabled = Device.cordova();
    me.getViewModel().set('isCordova', cordovaEnabled);
  },

  /**
   * On button click force a click on the hidden file input button
   * @return {void}
   * @private
   */
  onUploadClick () {
    const me = this;
    const uploadDomBtn = me.lookup('imageInputField').getFileButton().buttonElement.dom;
    uploadDomBtn.click();
  },

  /**
   * As new images are selected load them using the file reader API and update the image component
   * @param {Ext.Image} fileInput The input component
   * @return {void}
   * @private
   */
  onImageUpload (fileInput) {

    const me = this;

    if (fileInput.getFiles().length > 0 ) {
      const reader = new FileReader();
      const file = fileInput.getFiles()[0];
      reader.onload = (event) => {
        const imageCmp = me.lookup('image');
        imageCmp.setSrc(reader.result);

      }
      reader.readAsDataURL(file);
    }

  },

  /**
   * Select an image from the device gallery
   */
  onGallery () {

    const me = this;

    navigator.camera.getPicture(
      me.loadDataImage.bind(me),
      me.imageFail.bind(me),
      {
        quality: 50,
        destinationType: Camera.DestinationType.DATA_URL,
        sourceType: Camera.PictureSourceType.SAVEDPHOTOALBUM,
        correctOrientation: true
      }
    );

  },

  /**
   * Capture an image using the device camera
   */
  onCamera () {

    const me = this;

    navigator.camera.getPicture(
      me.loadDataImage.bind(me),
      me.imageFail.bind(me),
      {
        quality: 50,
        destinationType: Camera.DestinationType.DATA_URL,
        correctOrientation: true
      }
    );

  },

  loadDataImage (imageData) {
    const me = this;
    const imageCmp = me.lookup('image');
    imageCmp.setSrc('data:image/jpeg;base64,' + imageData);
  },

  /**
   * Helper function to load an image into the component
   * @param {string} image The URL or Data URL of the image to display
   */
  loadImage (image) {
    const me = this;
    const imageContainer = me.lookup('image');
    imageContainer.setSrc(image);
  },

  /**
   * Helper function to display an error if there was a problem getting the image
   * @param {string} message The error message
   */
  imageFail (message) {
    alert(`Error getting the image: ${message}`);
  }

});

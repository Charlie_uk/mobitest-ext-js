/**
 * Image capture example view
 */
Ext.define('Mobitest.view.examples.imagecapture.ImageCapture', {
  extend: 'Mobitest.view.examples.ExampleBase',
  xtype: 'mobi-exampleimagecapture',
  viewModel: true,
  controller: 'imagecapture',
  cls: 'mobi-exampleimagecapture',

  layout: {
    type: 'vbox',
    align: 'stretch'
  },

  scrollable: 'vertical',

  items: [
    {
      xtype: 'mobi-infopanel',
      title: 'Image Capture Demo',
      html: `
        <p>
          The options available for image capture should represent the capabilites of the device.
          When running on a mobile device the option to capture an image from a camera should be
          present along with the option to upload from the device gallery. When running in a web
          browser the only option should be to upload an image.
        </p>
      `
    },
    {
      xtype: 'filefield',
      label: "MyPhoto:",
      reference: 'imageInputField',
      accept: 'image',
      hidden: true, // Click on this component is simluated through code to start the file upload
      listeners: {
        change: 'onImageUpload'
      }
    },
    {
      xtype: 'panel',
      layout: 'fit',
      flex: 1,
      minHeight: 300,
      margin: '20 0 0 0',
      cls: 'mobi-imagecontainer',
      items: [
        {
          xtype: 'titlebar',
          title: 'Select Image',
          titleAlign: 'left',
          docked: 'top',
          items: [
            {
              xtype: 'segmentedbutton',
              align: 'right',
              allowToggle: false,
              items: [
                {
                  text: 'Upload Image',
                  iconCls: 'x-fa fa-image',
                  bind: {
                    hidden: '{isCordova}'
                  },
                  handler: 'onUploadClick'
                },
                {
                  text: 'Gallery',
                  iconCls: 'x-fa fa-image',
                  bind: {
                    hidden: '{!isCordova}'
                  },
                  handler: 'onGallery'
                },
                {
                  text: 'Camera',
                  iconCls: 'x-fa fa-camera',
                  bind: {
                    hidden: '{!isCordova}'
                  },
                  handler: 'onCamera'
                }
              ]
            }
          ]
        },
        {
          xtype: 'image',
          reference: 'image',
          margin: '10 0 10 0',
          width: '100%',
          height: '100%'
        }
      ]
    }
  ]

});

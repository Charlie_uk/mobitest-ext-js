/**
 * Example base class - Examples should extend this class to inherit common styling.
 */
Ext.define('Mobitest.view.examples.tab.Tab', {
  extend: 'Mobitest.view.examples.ExampleBase',
  xtype: 'mobi-exampletab',
  controller: 'tab',
  cls: 'mobi-exampletab',
  layout: 'vbox',

  requires: [
    'Mobitest.view.examples.tab.FormScroller'
  ],

  items: [
    {
      xtype: 'mobi-infopanel',
      title: 'Dialog Demo',
      html: `
        <p>WIP - Attempt to add extra information on tab overflow.</p>
      `,
      margin: '0 0 20 0'
    },
    {
      xtype: 'tabpanel',
      title: 'A Title',
      flex: 1,
      tabBar: {
        layout: {
            pack: 'start',
            overflow: 'formscroller'
        },
        defaults: {
          minWidth: 100 // Larger width to cause overflow without too much label clipping
        }
      },
      items: [
        {
          title: 'Tab One',
          html: 'Tab One'
        },
        {
          title: 'Tab Two',
          html: 'Tab Two'
        },
        {
          title: 'Tab Three',
          html: 'Tab Three'
        },
        {
          title: 'Tab Four',
          html: 'TabFour'
        },
        {
          title: 'Tab Five',
          html: 'Tab Five'
        },
        {
          title: 'Tab Six',
          html: 'Tab Six'
        }
      ]
    }
  ]

});

/**
 * Controller for the application home screen
 */
Ext.define('Mobitest.view.home.examples.tab.TabController', {
  extend: 'Ext.app.ViewController',
  alias: 'controller.tab'

});

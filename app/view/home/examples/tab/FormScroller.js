/**
 * Example base class - Examples should extend this class to inherit common styling.
 */
Ext.define('Mobitest.view.examples.tab.FormScroller', {
  extend: 'Ext.layout.overflow.Scroller',
  alias: 'layout.overflow.formscroller',

  config: {
    formDetailTool: {
      xtype: 'tool',
      ui: 'boxscroller',
      iconCls: 'fa-jedi',
      focusable: false
    }
  },


  applyFormDetailTool: function (tool, oldTool) {
    var ct = this.getContainer();

    tool = Ext.updateWidget(oldTool, tool, this, 'createFormDetailTool');
    delete tool.$initParent;
    tool.ownerCmp = ct;

    // TODO -- What are these doing?
    tool.doInheritUi();
    tool.addUi('boxscroller-' + ct.xtype);

    return tool;
  },

  createFormDetailTool: function (config) {
    var me = this;

    return Ext.apply({
        $initParent: me.getContainer(),
        hidden: true,
        preventRefocus: true
    }, config);

  }

});

Ext.define('Mobitest.view.menu.SideMenu', {
  extend: 'Ext.Panel',
  xtype: 'sidemenu',

  config: {
    menuOptions: null
  },

  cls: 'sidemenu',
  ui: 'mobi-sidepanel',

  width: '100%',

  layout: {
    type:'vbox',
    align: 'stretch'
  },

  items: [
    {
      xtype: 'container',
      itemId: 'menuoptions',
      cls: 'sidemenu-menuoptions',
      layout: {
        type: 'vbox',
        pack: 'start',
        align: 'stretch'
      },
      flex: 1,
      defaults: {
        xtype: 'button',
        textAlign: 'left',
        ui: 'sidemenu'
      },
    },
    {
      xtype: 'container',
      itemId: 'systemoptions',
      cls: 'sidemenu-systemoptions',
      layout: {
        type: 'vbox',
        pack: 'end',
      },
      defaults: {
        xtype: 'button',
        textAlign: 'left',
        ui: 'sidemenu'
      },
      height: 50,
      items: [
        {
          xtype: 'button',
          text: 'logout',
          iconCls: 'x-fa fa-power-off',
          handler: function (btn) {
            btn.up('sidemenu').fireEvent('logout');
          }
        }
      ]
    }
  ],



  /**
   * Update the menu options attached to this component. For each menu item in the store a menu
   * button is created. Does not attempt to react to changes in the store currently.
   *
   * TODO - Use a componentdataview to bind to the store items, this would make the view reactive
   * to changes in the store.
   *
   * @param {Ext.data.Store} options Store of menu options that this component should render
   */
  updateMenuOptions (menuOptions) {

    console.log('update menuOptions');

    const me = this
    const menuOptionsContainer = me.getComponent('menuoptions');

    menuOptionsContainer.removeAll();

    if (!menuOptions) { return; }

    menuOptions.each(option => {
      menuOptionsContainer.add({
        text: option.get('text'),
        iconCls: `x-fa ${option.get('icon')}`,
        handler: function (btn) {
          btn.up('sidemenu').fireEvent('menuItemSelected', option);
        }
      });
    });

  },

});

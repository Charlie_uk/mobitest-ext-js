Ext.define('Mobitest.view.main.MainView', {
  extend: 'Ext.Container',
  xtype: 'mainview',

  controller: 'mainviewcontroller',
  viewModel: 'mainviewmodel',

  cls: 'mainview',
  layout: {
    type:'card',
    animation : 'pop'
  },

  items: [
    {
      xtype: 'authlogin',
      reference: 'login',
      listeners: {
        'login': 'onLogin'
      }
    },
    {
      xtype: 'home',
      reference: 'home',
      listeners: {
        'logout': 'onLogout'
      }
    }
  ]

});

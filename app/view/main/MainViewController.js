Ext.define('Mobitest.view.main.MainViewController', {
  extend: 'Ext.app.ViewController',
  alias: 'controller.mainviewcontroller',

  routes : {
    'login': 'onRouteLogin',
    'home': {
      before  : 'onBeforeRouteHome',
      action  : 'onRouteHome'
    }
  },

  /**
   * Set the active item on startup to avoid flicker when already authenticated.
   */
  init () {
    const me = this;
    const home = me.lookup('home');
    if (UserSession.isAuthenticated()) {
      me.getView().setActiveItem(home);
    }
  },

  /*
   * -------------- Route Handlers --------------
   */

  onBeforeRouteHome (action) {
    const me = this;
    if (UserSession.isAuthenticated()) {
      action.resume();
    } else {
      action.stop();
      me.redirectTo('login');
    }
  },

  onRouteLogin () {
    const me = this;
    const login = this.lookup('login');
    me.getView().setActiveItem(login);
  },

  onRouteHome () {
    const me = this;
    const login = this.lookup('home');
    me.getView().setActiveItem(login);
  },

  /*
   * -------------- Listeners --------------
   */

  onLogin () {
    this.redirectTo('home');
  },

  onLogout () {

    const me = this;
    const dialog = Ext.create({
      xtype: 'dialog',
      plugins: {
        responsive: true
      },
        responsiveConfig: {
        'width < 768': { maximized: true },
        'width >= 768': { maximized: false }
      },
      title: 'Logout',
      html: `
        <p>Are you sure you wish to logout?</p>
      `,
      buttons: {
        ok: function () {
          UserSession.logout();
          me.redirectTo('login');
          dialog.destroy();
        },
        cancel: function () {
          dialog.destroy();
        }
      }
    });

    dialog.show();

  }

});

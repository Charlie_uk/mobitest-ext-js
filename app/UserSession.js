Ext.define('Mobitest.UserSession', {
  extend: 'Ext.data.Model',
  alternateClassName: 'UserSession',
  singleton: true,

  requires: ['Mobitest.util.Storage'],

  _sessionKey: 'UserSession',

  fields: [
    { name: 'token', type: 'string', allowNull: true, defaultValue: null },
    { name: 'userId', type: 'string', allowNull: true, defaultValue: null },
    { name: 'userDisplayName', type: 'string', allowNull: true, defaultValue: null },
    { name: 'permissions', type: 'auto', allowNull: true, defaultValue: null },
    { name: 'expiresAt', type: 'date', allowNull: true, defaultValue: null }
  ],

  /**
   * On initialising the class any valid persisted session should be restored.
   * @returns {void}
   * @private
   */
  init () {
    this.restoreSession();
  },

  /**
   * Restore a saved session from persisted storage. The session must be revalidated before it can
   * be restored, if the persisted session has expired then it is removed and not restored.
   * @returns {void}
   * @private
   */
  restoreSession() {

    const me = this;
    const previousSessionData = PersistantStore.get(this._sessionKey);

    if (!previousSessionData) return;

    const previousSession = JSON.parse(previousSessionData);

    if (me.isExpired(previousSession)) {
      me.logout(); // blanks this session and removes the persisted session
    } else {
      me.set(previousSession);
    }

  },

  /**
   * Persists details of the current user session to storage so that the user is not requried to
   * login in again until the session expires or they choose to logout.
   * @param {string} sessionData The data to persist for this session
   * @returns {void}
   * @private
   */
  persistSession(sessionData) {
    PersistantStore.set(this._sessionKey, JSON.stringify(sessionData));
  },


  /**
   * Logouts out the current user by clearing the current session and removing the session from
   * persistant storage.
   * @returns {void}
   */
  logout() {
    const me = this;
    me.set({
      token: null,
      userId: null,
      userDisplayName: null,
      permissions: null,
      expiresAt: null
    });
    PersistantStore.delete(this._sessionKey);
  },

  /**
   * Checks if the session expiry datetime supplied has expired
   * @param {string} date A date string
   * @returns {boolean}
   * @private
   */
  isExpired (date) {
    const now = new Date();
    const sessionDate = new Date(date);
    return sessionDate <= now;
  },

  /**
   * Returns true if the user is authenticated.
   */
  isAuthenticated() {
    const me = this;
    return me.get('token') && !me.isExpired(me.get('expiresAt'));
  },

  /**
   * Authentics a user and setups up the session if sucessful, otherwise an errors object is returned.
   * @param {string} username     username
   * @param {string} password     password
   * @returns {Ext.Promise}       Promise returns null if login sucesdful or an errors object otherwise
   */
  login(username, password) {

    const me = this;

    return new Ext.Promise(function (resolve, reject) {

      // Test user data
      users = [{
        token: 'ABCD123',
        username: 'admin',
        password: 'bossman',
        displayName: 'Admin 001',
        id: "001",
        permissions: ['ADMIN']
      }]

      // simulate a network wait
      setTimeout(() => {

        const user = Ext.Array.findBy(this.users, (user) => {
          return user.username === username && user.password === password
        });

        if (!user) {
          return reject({
            username: 'Login failed: invalid username or password'
          });
        }

        const sessionData = {
          token: user.token,
          userId: user.id,
          userDisplayName: user.displayName,
          permissions: user.permissions,
          expiresAt: moment().clone().add(1, 'days').toDate()
        }

        me.set(sessionData);
        me.persistSession(sessionData);

        resolve(null);

      }, 1500);

    });

  }

});
Ext.define('Mobitest.Application', {
    extend: 'Ext.app.Application',
    name: 'Mobitest',
    requires: ['Mobitest.*', 'Ext.plugin.Responsive'],

    defaultToken : 'home',

    launch: function () {
      Ext.Viewport.add([{xtype: 'mainview'}])
    },

    onAppUpdate: function () {
      Ext.Msg.confirm('Application Update', 'This application has an update, reload?',
        function (choice) {
          if (choice === 'yes') {
            window.location.reload()
          }
        }
      )
    }
  })

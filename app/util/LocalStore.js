Ext.define('Mobitest.util.Storage', {
  alternateClassName: 'PersistantStore',
  singleton: true,

  /*
   * Uses local store for simple examples, indexDB would be prefferable with large complex data
   * although it has limitations on windows devices
   */

  get (key) {
    return window.localStorage.getItem(key);
  },

  set (key, value) {
    return window.localStorage.setItem(key, value);
  },

  delete (key) {
    return window.localStorage.removeItem(key);
  }

});

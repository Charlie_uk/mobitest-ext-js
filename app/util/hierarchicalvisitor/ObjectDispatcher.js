/**
 * This class implements a visitor dispatcher for hierarchical JavaScript object structures.
 * Objects cannot directly implement an "accept" function as per the traditional pattern
 * implementaion. Also JSON structures of objects are generally untyped and so extra though has to
 * be put given to identifying each type of object in the vistor.
 *
 * This dispatcher visits each item of all array properties of the current node. The visitor itself
 * should decided if the array items should be processed (using enter and leave functions).
 */
Ext.define('Mobitest.util.hierarchicalvisitor.ObjectDispatcher', {

  /**
   * Visit each node in this tree
   * @param {Object} node A single root node object in a tree hierachy of objects
   * @param {Mobitest.util.hierarchicalvisitor.Visitor} visitor The visitor to visits each object
   * in the tree hierarachy
   * @param {context} context A context object, can be used to help due to anoymouse nature of
   * JavaScript objects
   */
  dispatch (node, visitor, context) {

    const me = this;

    if (visitor.beginVisit(node, context)) {

      visitor.visit(node, context);

      // Visit any children, assumes arrays contain child objects
      Object.keys(node).forEach(function(key) {
        let property = node[key];
        if (Array.isArray(property)) {
          for (let i = 0; i < property.length; i++) {
            const continueProcessing = me.dispatch(property[i], visitor, { parentNode: node, propertyName: key });
            if (!continueProcessing) break; // Don't process siblings
          }
        }
      });

    }

    return visitor.endVisit(node, context);

  }

});
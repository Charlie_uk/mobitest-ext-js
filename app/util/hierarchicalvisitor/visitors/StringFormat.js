/**
 * This visitor converts the tree to a basic string representation.
 */
Ext.define('Mobitest.util.hierarchicalvisitor.visitors.StringFormat', {
  extend: 'Mobitest.util.hierarchicalvisitor.Visitor',

  indent: 0,
  indentIncrememt: 2,
  result: '',

  nodeFunctions: {
    Person: function (person) {
      this.result = `${this.result}${this.getSpaces(this.indent)}${person.surname}, ${person.name} (${person.age||'unknown'})\n`;
    },
    Trait: function (trait) {
      this.result = `${this.result}${this.getSpaces(this.indent)}${this.getTrait(trait, 'en')}\n`;
    }
  },

  /**
   * increase the indent
   * @param {any} node The node in the tree being vistited
   * @param {Object} context An object that kind provide extra contextual information
   * @returns {void}
   * @private
   */
  beginVisit (node, context) {
    this.indent = this.indent + this.indentIncrememt;
    return true;
  },

  /**
   * decrease the indent
   * @param {any} node The node in the tree being vistited
   * @param {Object} context An object that kind provide extra contextual information
   * @returns {void}
   * @private
   */
  endVisit (node, context) {
    this.indent = this.indent - this.indentIncrememt;
    return true;
  },

  /**
   * Attempts to infer the type of this node, due to javascript objects being anoymous in nature
   * extra contextual information is included (the containing property name) that allows the type
   * to be inferred.
   * @param {any} node The node in the tree being vistited
   * @param {Object} context An object that kind provide extra contextual information
   * @returns {String}
   * @private
   */
  getType (node, context) {
    if (Ext.isObject(node) && node.name) return 'Person';
    if (context && context.propertyName === 'traits') return 'Trait';
  },

  /**
   * Call the correct function to create a string representation of this node
   * @param {any} node The node in the tree being vistited
   * @param {Object} context An object that kind provide extra contextual information
   * @private
   * @returns {void}
   */
  visit (node, context) {
    const me = this;
    const nodeType = me.getType(node, context);
    if (nodeType) {
      me.nodeFunctions[nodeType].bind(me)(node);
    }
  },

  /**
   * Returns the result of this visitor being applied against a data structure
   * @returns {String}
   */
  getResult () {
    return this.result;
  },

  /**
   * Returns a string that is the number of spaces requested
   * @param {Number} count the number of spaces to return
   */
  getSpaces (count = 0) {
    let spaces = '';
    for (let step = 0; step < count; step++) {
      spaces += ' ';
    }
    return spaces;
  },

  /**
   * Map a trait code to a textual representation
   * @param {String} traitCode the trait code
   * @param {String} lang the trait language
   * @returns {String} Textual trait representation
   * @private
   */
  getTrait(traitCode, lang) {
    const traits = {
      A: { "en": "strong", "de": "stark" },
      B: { "en": "polite", "de": "freundlich" },
      C: { "en": "loyal", "de": "loyal" },
      D: { "en": "beautiful", "de": "schön" },
      E: { "en": "sneaky", "de": "hinterlistig" },
      F: { "en": "experienced", "de": "erfahren" },
      G: { "en": "corrupt", "de": "korrupt" },
      H: { "en": "powerful", "de": "einflussreich" },
      I: { "en": "naive", "de": "naiv" },
      J: { "en": "unmarried", "de": "unverheiratet" },
      K: { "en": "skillful", "de": "geschickt" },
      L: { "en": "young", "de": "jung" },
      M: { "en": "smart", "de": "klug" },
      N: { "en": "rational", "de": "rational" },
      O: { "en": "ruthless", "de": "skrupellos" },
      P: { "en": "brave", "de": "mutig" },
      Q: { "en": "mighty", "de": "mächtig" },
      R: { "en": "weak", "de": "schwach" }
    }
    return traits[traitCode][lang];
  }

});
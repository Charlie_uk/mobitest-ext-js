/**
 * This is the abstarct class implements for a vistor class to be used in the  hierarchical tree
 * vistor pattern.
 *
 * @abstract
 */
Ext.define('Mobitest.util.hierarchicalvisitor.Visitor', {

  /**
   * Called before a node is visited. Provides an opporunity to assess if the node visit method
   * should be called. Provides opportunity to setup any local config before the the visit method
   * is caleld.
   *
   * Returning true indicates the node visit method will be called
   * Returning false indicates the node visit method will not be called
   *
   * @param {any} node The node in the tree being vistited
   * @param {Object} context An object that kind provide extra contextual information
   * @returns {boolean} if false this node is not visited
   * @abstract
   */
  beginVisit (node, context) {
    return true; // Not truely abstarct (not possible with Ext), default implementation.
  },

  /**
   *
   *
   * @param {any} node The node in the tree being vistited
   * @param {Object} context An object that kind provide extra contextual information
   * @returns {void}
   * @abstract
   */
  visit (node, context) {
  },

  /**
   * Called after a node (and any children) is visited. This is called regardless of if the visit
   * method was called. Provides an opportunity to terminate processing of this branch and to
   * tear down any local config setup specifically for the processing of this branch.
   *
   * Returning true indicates branch processing should continue. Siblings will be visited.
   * Returning false indicates branch processing should terminate. Siblings will not be visited.
   *
   * @param {any} node The node in the tree being vistited
   * @param {Object} context An object that kind provide extra contextual information
   * @returns {boolean} if false indicates that processing this branch should terminate
   * @abstract
   */
  endVisit (node, context) {
    return true // Not truely abstarct (not possible with Ext), default implementation.
  }

});
Ext.define('Mobitest.util.Device', {
  alternateClassName: 'Device',
  singleton: true,

  /** return true or false if cordova is enabled */
  cordova () {
    return window.hasOwnProperty('cordova');
  }

});

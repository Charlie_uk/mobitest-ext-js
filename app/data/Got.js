/**
 * Example hierarchical Game of thrones test data
 */
Ext.define('Mobitest.data.Got', {

  statics: {
    info: {
      characters: [
        {
          "_key": "NedStark",
          "name": "Ned",
          "surname": "Stark",
          "alive": true,
          "age": 41,
          "traits": ["A", "H", "C", "N", "P"],
          "children": [
            {
              "_key": "SansaStark",
              "name": "Sansa",
              "surname": "Stark",
              "alive": true,
              "age": 13,
              "traits": ["D", "I", "J"]
            },
            {
              "_key": "AryaStark",
              "name": "Arya",
              "surname": "Stark",
              "alive": true,
              "age": 11,
              "traits": ["C", "K", "L"]
            },
            {
              "_key": "RobbStark",
              "name": "Robb",
              "surname": "Stark",
              "alive": false,
              "traits": ["A", "B", "C", "K"]
            },
            {
              "_key": "BranStark",
              "name": "Bran",
              "surname": "Stark",
              "alive": true,
              "age": 10,
              "traits": ["L", "J"]
            },
            {
              "_key": "JonSnow",
              "name": "Jon",
              "surname": "Snow",
              "alive": true,
              "age": 16,
              "traits": ["A", "B", "C", "F"]
            }
          ]
        },
        {
          "_key": "TywinLannister",
          "name": "Tywin",
          "surname": "Lannister",
          "alive": false,
          "traits": ["O", "M", "H", "F"],
          "children": [
            {
              "_key": "CerseiLannister",
              "name": "Cersei",
              "surname": "Lannister",
              "alive": true,
              "age": 36,
              "traits": ["H", "E", "F"],
              "children": [
                {
                  "_key": "JoffreyBaratheon",
                  "name": "Joffrey",
                  "surname": "Baratheon",
                  "alive": false,
                  "age": 19,
                  "traits": ["I", "L", "O"]
                },
                {
                  "_key": "TommenBaratheon",
                  "name": "Tommen",
                  "surname": "Baratheon",
                  "alive": true,
                  "traits": ["I", "L", "B"]
                }
              ]
            },
            {
              "_key": "TyrionLannister",
              "name": "Tyrion",
              "surname": "Lannister",
              "alive": true,
              "age": 32,
              "traits": ["F", "K", "M", "N"]
            },
            {
              "_key": "JaimeLannister",
              "name": "Jaime",
              "surname": "Lannister",
              "alive": true,
              "age": 36,
              "traits": ["A", "F", "B"]
            }
          ]
        }
      ]
    }
  }

});
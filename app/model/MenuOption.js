Ext.define('Mobitest.model.MenuOption', {
  extend: 'Ext.data.Model',

  fields: [
      { name: 'text',  type: 'string'},
      { name: 'icon',  type: 'string'},
      { name: 'xtype', type: 'string'}
  ]

});
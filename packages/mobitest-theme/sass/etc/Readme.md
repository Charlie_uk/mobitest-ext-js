# mobitest-theme/sass/etc

This folder contains miscellaneous SASS files. Unlike `"mobitest-theme/sass/etc"`, these files
need to be used explicitly.

# mobitest-theme/sass

This folder contains SASS files of various kinds, organized in sub-folders:

    mobitest-theme/sass/etc
    mobitest-theme/sass/src
    mobitest-theme/sass/var

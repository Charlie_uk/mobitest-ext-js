# Mobitest

The app was built to demonstrating different ways that an Ext JS applicaiton can be made reactive to changes in device size and plaform.

This covers the following errors currently :

* **App menu** - Fixed to the home screen on larger devices but switches to a slideout action sheet on mobile phones or portrait tablets.
* **Forms** - Demonstrates label realignment at smaller sizes.
* **Dialog** - Demonstrates dialog switching between fullscreen and a fixed size on smaller screen sizes. This is useful to avoid sizing and reorientation issues on smaller devices.
* **Field Containers** - Demonstrates a strategy for collapsing horizontally grouped from fields when on a smaller device.
* **Image Capture** - Demonstrates a way to offer camera and gallery upload on Cordova enabled devices, and image upload when running in a desktop browser

## Installation

You will need to provide a version of Ext in the "/ext" folder at the application root. The application was developed using Ext 6.6 and Cmd 6.6.0 but may work with earlier versions.
The application should work with the community version of Ext but the folder structure is different so may need to copy the application into the generated app folder if you wish to use the community edition.

## Theme

The application uses a custom theme based on the material theme provided by Sencha. All styling is done by proving component UIs in the theme folder `packages/mobitest-theme' or with component sass files that sit along side each styled component.

## Phonegap

The application has been initialise for remote building with phonegap build. The is has only been setup for building android apks currently.

### Prerequisites

If you wish to build the application remotely you will need to register for an account at [phonegap build](https://build.phonegap.com/).
You will then need to create a local.properties file at the root of the project which contains your username and login details.

You will need to initialise the project for phone gap full instructions can be found in the [Sencha help pages](http://docs.sencha.com/cmd/6.6.0/guides/cordova_phonegap.html).

#### TLD

```bash
 sudo npm install -g phonegap
 sencha phonegap init com.myDomain.MobiTest MobiTest
```

### Build command

```bash
sencha app build android
```

NOTE PHONEGAP ANDROID BUILD WITH LATER VERSIONS OF NODE SEEMS TO FAIL - Node 8 worked on last test
